package com.energypediaconsult.microstreamrestart;

import java.util.ArrayList;

public class DataRoot {
  private ArrayList<String> data = new ArrayList<>();

  public ArrayList<String> getData() {
    return this.data;
  };

  public void add(String item) {
    this.data.add(item);
  }
}

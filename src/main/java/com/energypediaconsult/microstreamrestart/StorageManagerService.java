package com.energypediaconsult.microstreamrestart;

import one.microstream.storage.embedded.types.EmbeddedStorageManager;

public class StorageManagerService {
  private EmbeddedStorageManager storageManager;
  
  public StorageManagerService(StorageManagerFactory storageManagerFactory) {
    this.storageManager = storageManagerFactory.create();
    this.storageManager.start();
  }

  public EmbeddedStorageManager getStorageManager() {
    return this.storageManager;
  }

  public DataRoot getDataRoot() {
    return (DataRoot) this.storageManager.root();
  }
}

package com.energypediaconsult.microstreamrestart;

import java.nio.file.Path;

public class EmbeddedStorageManagerFactory extends StorageManagerFactory {
  private final String storageDir = "data";
  protected Path getStorageDirAsPath() {
    return Path.of(this.storageDir);
  }
}

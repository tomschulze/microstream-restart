package com.energypediaconsult.microstreamrestart;

import java.nio.file.Path;

import one.microstream.storage.embedded.types.EmbeddedStorage;
import one.microstream.storage.embedded.types.EmbeddedStorageManager;

public abstract class StorageManagerFactory {
  protected final static DataRoot ROOT = new DataRoot();

  public EmbeddedStorageManager create() {
    Path storageDir = this.getStorageDirAsPath();

    return EmbeddedStorage.Foundation(storageDir)
    .setRoot(StorageManagerFactory.ROOT)
    .createEmbeddedStorageManager();
  };

  abstract protected Path getStorageDirAsPath();
}

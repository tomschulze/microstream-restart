package com.energypediaconsult.microstreamrestart;

import java.nio.file.FileSystem;
import java.nio.file.Path;

import com.google.common.jimfs.Jimfs;

public class MockStorageManagerFactory extends StorageManagerFactory {
  private final static FileSystem FILESYSTEM = Jimfs.newFileSystem();
  private final static String STORAGE_DIR = "data";

  protected Path getStorageDirAsPath() {
    return FILESYSTEM.getPath(STORAGE_DIR);
  }
}

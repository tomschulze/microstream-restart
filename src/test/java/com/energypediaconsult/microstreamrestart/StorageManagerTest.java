package com.energypediaconsult.microstreamrestart;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import one.microstream.storage.embedded.types.EmbeddedStorageManager;

public class StorageManagerTest {

  EmbeddedStorageManager storageManager;

  @BeforeEach
  public void createStorageManager() {
    StorageManagerService storeManagerService = new StorageManagerService(new MockStorageManagerFactory());
    storageManager = storeManagerService.getStorageManager();
  }

  @Test
  public void testStartStore() {
    assertEquals(true, storageManager.isRunning());
  }

  @Test
  public void testIsShutdown() {
    assertEquals(true, storageManager.isRunning());
    assertEquals(false, storageManager.isShutdown());

    storageManager.shutdown();

    assertEquals(false, storageManager.isRunning());
    assertEquals(true, storageManager.isShutdown());
  }

  @Test
  public void testCanRestart() {
    assertEquals(true, storageManager.isRunning());
    assertEquals(false, storageManager.isShutdown());

    storageManager.shutdown();

    assertEquals(false, storageManager.isRunning());
    assertEquals(true, storageManager.isShutdown());

    storageManager.start();

    assertEquals(true, storageManager.isRunning());
    assertEquals(false, storageManager.isShutdown());
  }
}

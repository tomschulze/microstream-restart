Is it possible to somehow run `shutdown()` and then `start()` again on the same `EmbeddedStorageManager` instance?

Running

```
storageManager.shutdown();
storageManager.start();
```

throws the following exception:


Stacktrace
```
one.microstream.storage.exceptions.StorageExceptionConsistency: No roots found for existing data.
 at one.microstream.storage.embedded.types.EmbeddedStorageManager$Default.validateEmptyDatabaseAndReturnDefinedRoots(EmbeddedStorageManager.java:354)
 at one.microstream.storage.embedded.types.EmbeddedStorageManager$Default.initialize(EmbeddedStorageManager.java:370)
 at one.microstream.storage.embedded.types.EmbeddedStorageManager$Default.start(EmbeddedStorageManager.java:251)
 at one.microstream.storage.embedded.types.EmbeddedStorageManager$Default.start(EmbeddedStorageManager.java:1)
 at com.energypediaconsult.microstreamrestart.StorageManagerTest.testCanRestart(StorageManagerTest.java:45)
 at java.base/java.util.ArrayList.forEach(Unknown Source)
 at java.base/java.util.ArrayList.forEach(Unknown Source)
 ```

 The [microstream docs](https://docs.microstream.one/manual/storage/application-life-cycle.html) say 

 >For example, it is perfectly valid to start the StorageManager, work with the database, then stop it, maybe change some configuration or copy files or something like that and then start it up again to continue working.

